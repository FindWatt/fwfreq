import os, sys, re

import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'FwFreq/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = re.findall(r"\d+\.\d+\.\d+", version_line)[0]

def download_nltk_stopwords():
    print("About to download nltk stopwords...")
    import nltk
    nltk.download("stopwords")
    nltk.download("punkt")

            
class custom_install(install):
    def run(self):
        print("This is a custom installation")
        install.run(self)
        download_nltk_stopwords()

setuptools.setup(
    name="FwFreq",
    version=__version__,
    url="https://bitbucket.org/FindWatt/fwfreq",

    author="FindWatt",

    description="Frequency Utilities",
    long_description=open('README.md').read(),

    packages = setuptools.find_packages(),
    package_data={'': ["*.pyx","*.txt"]},
    py_modules=['FwFreq'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "cython",
        "numpy",
        "pandas",
        "nltk",
    ],
    cmdclass={'install': custom_install}
)


